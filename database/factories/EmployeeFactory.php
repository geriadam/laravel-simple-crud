<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    $birth_date = $faker->dateTimeBetween($startDate = '-35 years', $endDate = '-20 years', $timezone = null);
    return [
        "full_name"  => $faker->name($gender = "male"),
        "nick_name"  => $faker->firstNameMale,
        "age"        => $faker->numberBetween($min = 20, $max = 35),
        "birth_date" => $birth_date->format('Y-m-d'),
        "address"    => $faker->address,
        "mobile"     => $faker->tollFreePhoneNumber,
        "avatar"     => "default.png",
        "created_by" => 1,
        "modify_by"  => 1
    ];
});
