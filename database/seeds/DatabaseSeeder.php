<?php

use App\User;
use App\Employee;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create(["email" => "admin@admin.com"]);
        factory(User::class, 50)->create();
        factory(Employee::class, 50)->create();
    }
}
