<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route Auth
Route::namespace('API')
    ->name('api.')
    ->prefix('auth')
    ->group(function () {

        // Login and Register
        Route::post('login', 'AuthController@login')->name('login');
        Route::post('signup', 'AuthController@signup')->name('signup');

        // Get Data
        Route::get('user', 'AuthController@user')->name('index');

        Route::middleware(['auth:api'])
            ->group(function () {
                // Logout
                Route::get('logout', 'AuthController@logout')->name('logout');
            });
    });

// Route Employee
Route::namespace('API')
    ->name('api.')
    ->middleware(['auth:api'])
    ->group(function(){
        Route::apiResource('employee', 'EmployeeController');
    });

// Route Reset Password
Route::namespace('API')
    ->name('api.password.')
    ->prefix('password')
    ->group(function(){
        Route::post('create', 'PasswordResetController@create')->name('create');
        Route::get('find/{token}', 'PasswordResetController@find')->name('find');
        Route::post('reset', 'PasswordResetController@reset')->name('reset');
    });
