@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Employee List</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ url('home') }}" method="get">
                        <div class="form-group">
                            <input
                                type="text"
                                name="q"
                                class="form-control"
                                placeholder="Search..."
                                value="{{ request('q') }}"
                            />
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Avatar</th>
                                <th>Full Name</th>
                                <th>Age</th>
                                <th>Birth Date</th>
                                <th>Address</th>
                            </thead>
                            <tbody>
                                @foreach($employees as $employee)
                                    <tr>
                                        <td><img width="100px" height="60px" src="{{ $employee->avatar_url }}"/></td>
                                        <td>{{ $employee->full_name }}</td>
                                        <td>{{ $employee->age }}</td>
                                        <td>{{ $employee->birth_date }}</td>
                                        <td>{{ $employee->address }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $employees->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
