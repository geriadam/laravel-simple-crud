<?php

namespace App;

use App\Models\Traits\CrudTrait;
use ScoutElastic\Searchable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends BaseModel
{
    use SoftDeletes;
    use CrudTrait;
    use Searchable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $indexConfigurator = MyIndexEmployee::class;
    protected $table = "employees";
    protected $fillable = [
        "full_name",
        "nick_name",
        "age",
        "birth_date",
        "address",
        "mobile",
        "avatar"
    ];
    protected $dates = ['deleted_at'];
    public $timestamps = true;


    const IMAGE_PATH   = "storage/employee/";

    // Here you can specify a mapping for model fields
    protected $mapping = [
        'properties' => [
            'full_name' => [
                'type' => 'text',
                // Also you can configure multi-fields, more details you can find here https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                    ]
                ]
            ],
        ]
    ];

    public function toSearchableArray()
    {
        return [
            "full_name" => $this->full_name
        ];
    }

    public function searchableAs()
    {
        return 'employee_index';
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getAvatarUrlAttribute()
    {
        if($this->avatar == 'default.png')
            return asset('img/no-image.jpg');

        return url(self::IMAGE_PATH . $this->avatar);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setAvatarAttribute($value)
    {
        $attribute_name = "avatar";
        $disk = "employee";
        $destination_path = "";

        // if the image was erased
        if ($value==null) {

            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        if($value !== null)
        {
            if(is_string($value)){
                $value = str_replace(self::IMAGE_PATH, "", $value);
                $this->attributes[$attribute_name] = $value;
            } else {
                $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
            }
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value)->encode('jpg', 90);

            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';

            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the public path to the database
            
            // but first, remove "public/" from the path, since we're pointing to it from the root folder
            
            // that way, what gets saved in the database is the user-accesible URL
            $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            $this->attributes[$attribute_name] = $public_destination_path.'/'.$filename;
        }
    }
}
