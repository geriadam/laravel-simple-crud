<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        $user = Auth::user();

        static::creating(function($item) use ($user) {
            if($user != null) {
                $user_id = $user->id;
                $item->created_by = $user_id;
                $item->modify_by = $user_id;
            }
        });

        static::updating(function($item) use ($user) {
            if($user != null) {
                $user_id = $user->id;
                $item->modify_by = $user_id;
            }
        });
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function modifyBy() {
        return $this->belongsTo(User::class, 'modify_by', 'id');
    }
}
