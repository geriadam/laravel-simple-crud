<?php

namespace App\Http\Controllers\Traits;

trait ResponseAPI
{
    /*
    |--------------------------------------------------------------------------
    | RESPONSE SUCCESS
    |--------------------------------------------------------------------------
    */

    public function sendResponse($result, $message, $code = 200)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data'    => $result
        ];

        return response()->json($response, $code, [], JSON_PRESERVE_ZERO_FRACTION);
    }

    /*
    |--------------------------------------------------------------------------
    | RESPONSE ERROR
    |--------------------------------------------------------------------------
    */

    public function sendError($message, $error = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $message,
        ];

        if (!empty($error)) {
            $response['data'] = $error;
        }
        
        return response()->json($response, $code);
    }

    public function sendValidationError($validation, $error = [], $code = 400) {
        return $this->sendError($validation->toArray(), $error, $code);
    }
}
