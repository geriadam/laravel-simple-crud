<?php

namespace App\Http\Controllers\API;

use App\User;
use App\PasswordReset;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ResponseAPI;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PasswordResetController extends Controller
{
    use ResponseAPI;

    /**
        @OA\Post(
            path="/api/password/create",
            tags={"password_create"},
            summary="Password Request",
            description="API Request Email Password",
            @OA\RequestBody(
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(
                        @OA\Property(
                            property="email",
                            type="string"
                        ),
                    )
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=422,
                description="Error: Unprocessable Entity"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
        )
    */
    public function create(Request $request)
    {
        $request->validate(['email' => 'required|string|email']);

        $user = User::where('email', $request->email)->first();
        if (!$user)
            return $this->sendError("We cant find a user with that e-mail address");
            
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(60)
            ]
        );

        if ($user && $passwordReset)
            $user->notify(new PasswordResetRequest($passwordReset->token));

        return $this->sendResponse([], "We have e-mailed your password reset link!", Response::HTTP_CREATED);
    }

    /**
        @OA\Get(
            path="/api/password/find/{token}",
            tags={"password_validate"},
            summary="Validate Token Reset Password",
            description="API Validate Token Reset Password",
            operationId="token",
            @OA\Parameter(
                name="token",
                description="Token Password",
                required=true,
                in="path",
                @OA\Schema(
                    type="string"
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
            security={
                {"bearerAuth": {}}
            }
        )
    */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset)
            return $this->sendError("This password reset token is invalid.");

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return $this->sendError("This password reset token is invalid.");
        }

        return $this->sendResponse($passwordReset, "Token is valid", Response::HTTP_OK);
    }

    /**
        @OA\Post(
            path="/api/password/reset",
            tags={"password_reset"},
            summary="Password Reset",
            description="API Reset Password",
            @OA\RequestBody(
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(
                        @OA\Property(
                            property="token",
                            type="string"
                        ),
                        @OA\Property(
                            property="email",
                            type="string"
                        ),
                        @OA\Property(
                            property="password",
                            type="string",
                            format="password"
                        ),
                        @OA\Property(
                            property="password_confirmation",
                            type="string",
                            format="password"
                        ),
                    )
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=422,
                description="Error: Unprocessable Entity"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
        )
    */
    public function reset(Request $request)
    {
        $request->validate([
            'email'    => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token'    => 'required|string'
        ]);

        $passwordReset = PasswordReset::where('token', $request->token)
            ->where('email', $request->email)
            ->first();

        if (!$passwordReset)
            return $this->sendError("This password reset token is invalid.");

        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return $this->sendError("We cant find a user with that e-mail address.");
        
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));

        return $this->sendResponse($user, "Successful reset password", Response::HTTP_OK);
    }
}
