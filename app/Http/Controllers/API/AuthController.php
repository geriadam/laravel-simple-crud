<?php

namespace App\Http\Controllers\API;

use Auth;
use DB;
use Hash;
use Carbon\Carbon;
use App\User;
use App\Constants\ResponseMessages;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ResponseAPI;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\SignupRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    use ResponseAPI;

    /**
        @OA\Post(
            path="/api/auth/signup",
            tags={"signup"},
            summary="Signup",
            description="API Signup",
            @OA\RequestBody(
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(
                        @OA\Property(
                            property="name",
                            type="string"
                        ),
                        @OA\Property(
                            property="email",
                            type="string"
                        ),
                        @OA\Property(
                            property="password",
                            type="string",
                            format="password"
                        ),
                        @OA\Property(
                            property="password_confirmation",
                            type="string",
                            format="password"
                        ),
                    )
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=422,
                description="Error: Unprocessable Entity"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
        )
    */
    public function signup(SignupRequest $request)
    {
        DB::begintransaction();

        try {

            $password = Hash::make($request->password);
            $user = User::create([
                "name"     => $request->name,
                "email"    => $request->email,
                "password" => $password
            ]);

            DB::commit();
            return $this->sendResponse(new UserResource($user), "Successful register", Response::HTTP_CREATED);

        } catch (Exception $e) {
            DB::rollback();
            return $this->sendError(ResponseMessages::RESPONSE_API_FAILED_CREATE);
        }
    }

    /**
        @OA\Post(
            path="/api/auth/login",
            tags={"login"},
            summary="Login",
            description="API Login",
            @OA\RequestBody(
                @OA\MediaType(
                    mediaType="application/json",
                    @OA\Schema(
                        @OA\Property(
                            property="email",
                            type="string"
                        ),
                        @OA\Property(
                            property="password",
                            type="string",
                            format="password"
                        ),
                        example={"email": "admin@admin.com", "password": "admin123"}
                    )
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
        )
    */
    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
            return $this->sendError("Email or Password is invalid", [], Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        $data = [
            'user'         => new UserResource($user),
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ];

        return $this->sendResponse($data, "Successful Login", Response::HTTP_OK);
    }

    /**
        @OA\Get(
            path="/api/auth/logout",
            tags={"auth_logout"},
            summary="User Logout",
            description="API User Logout",
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
            security={
                {"bearerAuth": {}}
            }
        )
    */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->sendResponse([], "Successful Logout", Response::HTTP_OK);
    }

    /**
        @OA\Get(
            path="/api/auth/user",
            tags={"auth_user"},
            summary="Show User Login",
            description="API Show User Login",
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
            security={
                {"bearerAuth": {}}
            }
        )
    */
    public function user(Request $request)
    {
        $user = auth('api')->user();
        return $this->sendResponse(new UserResource($user), ResponseMessages::RESPONSE_API_INDEX, Response::HTTP_OK);
    }
}
