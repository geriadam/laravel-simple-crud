<?php

namespace App\Http\Controllers\API;

use App\Employee;
use App\Search\EmployeeSearch\EmployeeSearch;
use App\Constants\ResponseMessages;
use App\Http\Requests\Employee\EmployeeCreateRequest;
use App\Http\Requests\Employee\EmployeeUpdateRequest;
use App\Http\Resources\EmployeeResource;
use App\Http\Resources\EmployeeCollection;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Response;

class EmployeeController extends Controller
{
    use ResponseAPI;

    /**
        @OA\Get(
            path="/api/employee",
            tags={"employee_index"},
            summary="List Employee",
            description="API Index Employee",
            operationId="employee",
            @OA\Parameter(
                name="full_name",
                description="Nama",
                required=false,
                in="query",
                @OA\Schema(
                    type="string"
                )
            ),
            @OA\Parameter(
                name="age",
                description="Umur",
                required=false,
                in="query",
                @OA\Schema(
                    type="integer"
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
            security={
                {"bearerAuth": {}}
            }
        )
    */
    public function index(Request $request)
    {
        $employees = Cache::remember("employee_index", 5, function () use ($request) {
            return EmployeeSearch::apply($request);
        });

        if($employees)
            return $this->sendResponse(new EmployeeCollection($employees), ResponseMessages::RESPONSE_API_INDEX, Response::HTTP_OK);

        return $this->sendError(ResponseMessages::RESPONSE_API_DATA_NOT_FOUND);
    }

    /**
        @OA\Post(
            path="/api/employee",
            tags={"employee_post"},
            summary="Post Employee",
            description="API Post Employee",
            @OA\RequestBody(
                @OA\MediaType(
                    mediaType="multipart/form-data",
                    @OA\Schema(
                        @OA\Property(
                            property="full_name",
                            type="string"
                        ),
                        @OA\Property(
                            property="nick_name",
                            type="string"
                        ),
                        @OA\Property(
                            property="age",
                            type="integer"
                        ),
                        @OA\Property(
                            property="birth_date",
                            type="string",
                            format="date"
                        ),
                        @OA\Property(
                            property="address",
                            type="string"
                        ),
                        @OA\Property(
                            property="mobile",
                            type="string"
                        ),
                        @OA\Property(
                            property="avatar",
                            type="file",
                            format="binary"
                        )
                    )
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
            security={
                {"bearerAuth": {}}
            },
        )
    */
    public function store(EmployeeCreateRequest $request)
    {
        $employee = Employee::create($request->all());

        if($employee)
            return $this->sendResponse(new EmployeeResource($employee), ResponseMessages::RESPONSE_API_CREATE, Response::HTTP_CREATED);

        return $this->sendError(ResponseMessages::RESPONSE_API_FAILED_CREATE);
    }

    /**
        @OA\Get(
            path="/api/employee/{id}",
            tags={"employee_show"},
            summary="Show Employee",
            description="API Show Employee",
            operationId="id",
            @OA\Parameter(
                name="id",
                description="Id Employee",
                required=true,
                in="path",
                @OA\Schema(
                    type="integer"
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
            security={
                {"bearerAuth": {}}
            }
        )
    */
    public function show(Employee $employee)
    {
        return $this->sendResponse(new EmployeeResource($employee), ResponseMessages::RESPONSE_API_INDEX, Response::HTTP_OK);
    }

    /**
        @OA\Put(
            path="/api/employee/{id}",
            tags={"employee_update"},
            summary="Update Employee",
            description="API Update Employee",
            operationId="id",
            @OA\Parameter(
                name="id",
                description="Id Employee",
                required=true,
                in="path",
                @OA\Schema(
                    type="integer"
                )
            ),
            @OA\RequestBody(
                @OA\MediaType(
                    mediaType="multipart/form-data",
                    @OA\Schema(
                        @OA\Property(
                            property="full_name",
                            type="string"
                        ),
                        @OA\Property(
                            property="nick_name",
                            type="string"
                        ),
                        @OA\Property(
                            property="age",
                            type="integer"
                        ),
                        @OA\Property(
                            property="birth_date",
                            type="string",
                            format="date"
                        ),
                        @OA\Property(
                            property="address",
                            type="string"
                        ),
                        @OA\Property(
                            property="mobile",
                            type="string"
                        ),
                        @OA\Property(
                            property="avatar",
                            type="file",
                            format="binary"
                        )
                    )
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
            security={
                {"bearerAuth": {}}
            }
        )
    */
    public function update(EmployeeUpdateRequest $request, Employee $employee)
    {
        $employee = $employee->update($request->all());

        if($employee)
            return $this->sendResponse(new EmployeeResource($employee), ResponseMessages::RESPONSE_API_UPDATE, Response::HTTP_OK);

        return $this->sendError(ResponseMessages::RESPONSE_API_FAILED_UPDATE);
    }

    /**
        @OA\Delete(
            path="/api/employee/{id}",
            tags={"employee_delete"},
            summary="Delete Employee",
            description="API Delete Employee",
            operationId="id",
            @OA\Parameter(
                name="id",
                description="Id Employee",
                required=true,
                in="path",
                @OA\Schema(
                    type="integer"
                )
            ),
            @OA\Response(
                response=200,
                description="Data Successful load",
                @OA\MediaType(
                    mediaType="application/json",
                )
            ),
            @OA\Response(
                response=401,
                description="Error: Unauthorized"
            ),
            @OA\Response(
                response=400,
                description="Invalid request"
            ),
            @OA\Response(
                response=404,
                description="Route not found"
            ),
            security={
                {"bearerAuth": {}}
            }
        )
    */
    public function destroy(Employee $employee)
    {
        if($employee->delete())
            return $this->sendResponse(new EmployeeResource($employee), ResponseMessages::RESPONSE_API_DELETE, Response::HTTP_OK);

        return $this->sendError(ResponseMessages::RESPONSE_API_FAILED_DELETE);            
    }
}
