<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->has('q') && !empty($request->q)){
            $employees = Employee::search($request->q)->paginate(10);    
        } else {
            $employees = Employee::paginate(10);
        }
        return view('home', compact('employees'));
    }
}
