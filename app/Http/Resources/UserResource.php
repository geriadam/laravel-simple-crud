<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'email'       => $this->email,
            'created_at'  => \Carbon\Carbon::parse($this->attributes['created_at'])->formatLocalized('%A, %d %B %Y %H:%I'),
            'updated_at'  => \Carbon\Carbon::parse($this->attributes['updated_at'])->formatLocalized('%A, %d %B %Y %H:%I')
        ];
    }
}
