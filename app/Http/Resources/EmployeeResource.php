<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"         => $this->id,
            "full_name"  => $this->full_name,
            "nick_name"  => $this->nick_name,
            "age"        => $this->age,
            "birth_date" => $this->birth_date,
            "address"    => $this->address,
            "mobile"     => $this->mobile,
            "avatar"     => $this->avatar_url,
            'created_at' => \Carbon\Carbon::parse($this->attributes['created_at'])->formatLocalized('%A, %d %B %Y %H:%I'),
            'updated_at' => \Carbon\Carbon::parse($this->attributes['updated_at'])->formatLocalized('%A, %d %B %Y %H:%I')
        ];
    }
}
