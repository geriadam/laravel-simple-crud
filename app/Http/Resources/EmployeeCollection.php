<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EmployeeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "result" => $this->collection->transform(function ($data) {
                return [
                    "id"         => $data->id,
                    "full_name"  => $data->full_name,
                    "nick_name"  => $data->nick_name,
                    "age"        => $data->age,
                    "birth_date" => $data->birth_date,
                    "address"    => $data->address,
                    "mobile"     => $data->mobile,
                    "avatar"     => $data->avatar_url,
                    'created_at' => \Carbon\Carbon::parse($data->attributes['created_at'])->formatLocalized('%A, %d %B %Y %H:%I'),
                    'updated_at' => \Carbon\Carbon::parse($data->attributes['updated_at'])->formatLocalized('%A, %d %B %Y %H:%I')
                ];
            }),
            "pagination" => [
                "current_page"   => $this->currentPage(),
                "per_page"       => $this->resource->toArray()['per_page'],
                "from"           => $this->resource->toArray()['from'],
                "to"             => $this->resource->toArray()['to'],
                "total"          => $this->resource->toArray()['total'],
                "last_page"      => $this->resource->toArray()['last_page'],
                "path"           => $this->resource->toArray()['path'],
                "first_page_url" => $this->resource->toArray()['first_page_url'],
                "last_page_url"  => $this->resource->toArray()['last_page_url'],
                "next_page_url"  => $this->resource->toArray()['next_page_url'],
                "prev_page_url"  => $this->resource->toArray()['prev_page_url'],
            ]
        ];
    }
}
