<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "full_name"  => "required|min:3|max:100|string",
            "nick_name"  => "required|min:4|max:30|string",
            "age"        => "required|integer",
            "birth_date" => "required|date_format:Y-m-d",
            "address"    => "required|string",
            "mobile"     => "required|string",
            "avatar"     => 'required|max:10000|mimes:jpg,jpeg,png',
        ];
    }
}
