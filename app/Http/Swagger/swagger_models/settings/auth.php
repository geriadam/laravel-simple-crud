<?php

/**
    @OA\SecurityScheme(
        type="http",
        in="header",
        securityScheme="bearerAuth",
        name="Authorization",
        bearerFormat="JWT",
        scheme="bearer",
        description="Oauth2 security",
        name="oauth2",
    )
*/
