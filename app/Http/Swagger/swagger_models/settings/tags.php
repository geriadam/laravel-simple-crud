<?php

/**
    @OA\Tag(
        name="signup",
        description="Signup User",
    ),
    @OA\Tag(
        name="login",
        description="Login User",
    ),
    @OA\Tag(
        name="auth_user",
        description="Data User Login",
    ),
    @OA\Tag(
        name="auth_logout",
        description="Logout User",
    ),
    @OA\Tag(
        name="password_create",
        description="Password Request",
    ),
    @OA\Tag(
        name="password_validate",
        description="Password Validate",
    )
    @OA\Tag(
        name="password_reset",
        description="Password Reset",
    ),
    @OA\Tag(
        name="employee_index",
        description="Employee - Index",
    ),
    @OA\Tag(
        name="employee_show",
        description="Employee - Show",
    ),
    @OA\Tag(
        name="employee_post",
        description="Employee - Post",
    ),
    @OA\Tag(
        name="employee_update",
        description="Employee - Update",
    ),
    @OA\Tag(
        name="employee_delete",
        description="Employee - Delete",
    )
**/

?>
