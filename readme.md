------------

## 💻 Install

**Clone Repository**
```bash
git clone https://geriadam@bitbucket.org/geriadam/laravel-simple-crud.git
cd laravel-simple-crud
composer install
cp .env.example .env
```

**Buka ```.env``` lalu setting sesuai configurasi**
```
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=tls

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=16380
REDIS_CLIENT=predis
REDIS_CACHE_DB=0

BROADCAST_DRIVER=log
CACHE_DRIVER=redis
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

SCOUT_DRIVER=elastic
SCOUT_QUEUE=true
```

**Instalasi website**
```bash
php artisan key:generate
php artisan migrate:fresh --seed
php artisan storage:link
```

**Jalankan website**
```bash
php artisan serve
```
